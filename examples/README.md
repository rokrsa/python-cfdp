# Examples

This folder contains examples for using the CFDP package.

First, start a a remote entity (server):

```
$ cd examples
$ python server.py
```

Then run one of the local entity (client) examples:

```
$ cd examples
$ python client_send_file_class1.py
```
